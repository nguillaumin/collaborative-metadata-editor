<?php

return [
    'title' => 'Upload assets',
    'drop_files' => 'Drop files here to upload',
    'collection_name' => 'Collection name',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'file_too_large' => 'File too large, must be < :limit MB',
];
