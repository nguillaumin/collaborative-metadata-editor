<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::orderBy('name');

        if (Auth::user()->role !== User::ROLE_ADMIN) {
            $tags = Tag::whereHas('assets', function (Builder $query) {
                return $query->whereHas('collection', function (Builder $query2) {
                    return $query2->whereHas('users', function (Builder $query3) {
                        return $query3->where('users.id', '=', Auth::user()->id);
                    });
                });
            });
        }
        $tags = $tags->get();

        return view('tags.index', [
            'tags' => $tags,
        ]);
    }

    public function show(Tag $tag)
    {
        $assets = Asset::whereHas('tags', function (Builder $query) use ($tag) {
            return $query->where('id', '=', $tag->id);
        });

        if (Auth::user()->role !== User::ROLE_ADMIN) {
            $assets = $assets->whereHas('collection', function (Builder $query) {
                return $query->whereHas('users', function (Builder $query2) {
                    return $query2->where('users.id', '=', Auth::user()->id);
                });
            });
        }

        $assets = $assets->orderBy('name')->get();

        return view('tags.show', [
            'tag' => $tag,
            'assets' => $assets,
        ]);
    }
}
