<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Collection;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CollectionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Collection::class, 'collection');
    }

    public function index(Request $request)
    {
        $assets = Asset::whereNull('collection_id')->get();

        $collections = Auth::user()->collections
            ->sortBy('name');

        if (Auth::user()->role === User::ROLE_ADMIN) {
            $collections = Collection::all()
                ->sortBy('name');
        }

        return view('collections.index', [
            'assets' => $assets,
            'collections' => $collections,
        ]);
    }

    public function show(Collection $collection)
    {
        $assets = Asset::where('collection_id', '=', $collection->id)
            ->orderBy('name')
            ->paginate(20);

        return view('collections.show', [
            'collection' => $collection,
            'assets'     => $assets,
        ]);
    }

    public function destroy(Collection $collection)
    {
        $collection->assets->each(function ($asset) {
            Storage::disk('public')->delete($asset->original_path);
            Storage::disk('public')->delete($asset->thumbnail_path);
            $asset->delete();
        });
        $collection->delete();

        return redirect()->route('collections.index');
    }
}
