@extends('layouts.app')

@section('title', "Invite someone")

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="bi bi-house"></i> <a href="{{ route('collections.index') }}">{{ __('nav.home') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">{{ __('nav.users') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Invite someone</li>
        </ol>
    </nav>

    <h1>Invite someone</h1>

    <form action="{{ route('users.send_invite') }}" method="post" class="mt-3">
        @csrf
        @method('POST')

        <div class="row mb-3">
            <label for="email" class="form-label col-md-4 text-md-end">Email address</label>
            <div class="col-md-5">
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email"
                    value="{{ old('email') }}" autofocus required>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>


        <div class="row mb-3">
            <label for="collections" class="form-label col-md-4 text-md-end">Collections</label>
            <div class="col-md-5">
                <select class="form-select @error('collection') is-invalid @enderror" id="collections" name="collections[]" required multiple>
                    @foreach ($collections as $collection)
                        <option value="{{ $collection->id }}">{{ $collection->name }}</option>
                    @endforeach
                </select>

                @error('collections')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>


        <div class="row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-success">Send invite</button>

                <a class="btn btn-link" href="{{ route('users.index') }}">
                    Cancel
                </a>
            </div>
        </div>

    </form>
@endsection
