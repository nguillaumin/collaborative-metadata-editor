<?php

return [
    'collection_count' => '{0}No collections|[1,*]:count collections',
    'photo_count' => '{0}No photos|[1,*]:count photos',
    'view' => 'View',
];
