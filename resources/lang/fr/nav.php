<?php

return [

    'home' => 'Accueil',

    'collections' => 'Collections',
    'upload' => 'Upload',
    'tags' => 'Tags',
    'users' => 'Utilisateurs',

    'previous' => 'Précédent',
    'next' => 'Suivant',
];
