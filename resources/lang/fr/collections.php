<?php

return [
    'collection_count' => '{0}Pas de collections|[1,*]:count collections',
    'photo_count' => '{0}Pas de photos|[1,*]:count photos',
    'view' => 'Voir',
];
