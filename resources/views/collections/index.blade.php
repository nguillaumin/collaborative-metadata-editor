@extends('layouts.app')

@section('title', "Collections")

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page"><i class="bi bi-house"></i> {{ __('nav.home') }}</li>
        </ol>
    </nav>

    <h1>{{ trans_choice('collections.collection_count', $collections->count()) }}</h1>

    <div class="mt-1 row row-cols-1 row-cols-sm-2 row-cols-xl-4 g-3">
        @foreach ($collections as $collection)
            @can('view', $collection)
                <div class="col d-flex">
                    <div class="card w-100">
                        @if ($collection->assets->isNotEmpty())
                            <a href="{{ route('collections.show', $collection) }}">
                                <img src="{{ $collection->assets->sortBy('name')->first()->thumbnail_url }}?ts={{ microtime(true) }}"
                                    class="card-img-top" alt="{{ $collection->name }}">
                            </a>
                        @endif
                        <div class="card-body">
                            <h5 class="card-title">{{ $collection->name }}</h5>
                            <p class="card-text">
                                {{ trans_choice('collections.photo_count', $collection->assets->count()) }}

                                @if ($collection->assets->pluck('processed')->contains(false))
                                    <i class="bi bi-exclamation-triangle-fill text-warning"></i>
                                @endif
                            </p>
                            <a href="{{ route('collections.show', $collection) }}">{{ __('collections.view') }}</a>
                        </div>
                    </div>
                </div>
            @endcan
        @endforeach
    </div>


@endsection
