const Tags = require("bootstrap5-tags/tags").default;

document.addEventListener("DOMContentLoaded", () => {
    document.querySelectorAll("select[name='tags[]']").forEach( el => {
        new Tags(el);
    });
});
