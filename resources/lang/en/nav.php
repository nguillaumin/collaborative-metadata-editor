<?php

return [

    'home' => 'Home',

    'collections' => 'Collections',
    'upload' => 'Upload',
    'tags' => 'Tags',
    'users' => 'Users',

    'previous' => 'Previous',
    'next' => 'Next',
];
