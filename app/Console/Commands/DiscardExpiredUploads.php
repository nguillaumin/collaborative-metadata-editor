<?php

namespace App\Console\Commands;

use App\Http\Controllers\UploadController;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DiscardExpiredUploads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploads:discard
        {--expiry= : Uploads older than the expiry will be discarded}
        {--delete : Actually discard the uploads. By default nothing is deleted}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Discard expired uploads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->option('expiry') && ! is_numeric($this->option('expiry'))) {
            $this->error('Invalid expiry "' . $this->option('expiry') . '". It must be a number of days');

            return 1;
        }

        $expiry = $this->option('expiry') ?: 5;

        $maxDate = Carbon::now()->subDays($expiry);

        $this->info("Discarding uploads older than {$expiry} days (before {$maxDate})");

        collect(Storage::disk('public')->directories(UploadController::UPLOADS_DIRECTORY))
            ->each(function ($dir) use ($maxDate) {
                $lm = new Carbon(Storage::disk('public')->lastModified($dir));
                if ($lm->lessThan($maxDate)) {
                    $msg = "Folder {$dir}' last modified {$lm} is before {$maxDate},";
                    if ($this->option('delete')) {
                        Storage::disk('public')->deleteDirectory($dir);
                        $this->info("{$msg} deleted");
                    } else {
                        $this->info("{$msg} would have been deleted");
                    }
                } else {
                    $this->info("Folder '{$dir}' last modified {$lm} is after {$maxDate}, skipping");
                }
            });

        return 0;
    }
}
