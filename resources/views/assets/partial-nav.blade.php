<nav>
    <ul class="pagination justify-content-center my-3">
        @if ($previous)
            <li class="page-item"><a class="page-link"
                    href="{{ route('assets.show', $previous) }}">{{ __('nav.previous') }}</a></li>
        @else
            <li class="page-item disabled"><a class="page-link">{{ __('nav.previous') }}</a></li>
        @endif
        @if ($next)
            <li class="page-item"><a class="page-link" href="{{ route('assets.show', $next) }}">{{ __('nav.next') }}</a></li>
        @else
            <li class="page-item disabled"><a class="page-link">{{ __('nav.next') }}</a></li>
        @endif
    </ul>
</nav>
