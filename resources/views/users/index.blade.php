@extends('layouts.app')

@section('title', 'Users')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="bi bi-house"></i> <a href="{{ route('collections.index') }}">{{ __('nav.home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('nav.users') }}</li>
        </ol>
    </nav>

    <h1>Users</h1>

    <a class="btn btn-primary float-end" href="{{ route('users.invite') }}">Invite someone</a>

    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Created</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>
                        <a href="{{ route('users.edit', $user) }}">
                            {{ $user->name }}
                        </a>
                        @if ($user->invitation_token)
                            <span class="ms-2 text-warning">
                                <i class="bi bi-card-heading"></i> Invite sent {{ $user->invited_at }}
                            </span>
                        @endif
                    </td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role ?? '-' }}</td>
                    <td>{{ $user->created_at }}</td>
                    <th>
                        <form action="{{ route('users.destroy', $user) }}" method="post"
                            onsubmit="javascript:return confirm('This user will be deleted');">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-link">
                                <i class="bi bi-trash text-danger"></i>
                            </button>
                        </form>
                    </th>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
