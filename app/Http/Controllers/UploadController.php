<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessNewAsset;
use App\Models\Collection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    const UPLOADS_DIRECTORY = 'uploads';

    public function index()
    {
        return view('upload.index', [
            'uploadId' => uniqid(),
        ]);
    }

    /**
     * Store a single uploaded file.
     *
     * @param  Request  $request  HTTP request
     */
    public function store(Request $request)
    {
        if ($request->has('file') && $request->has('id')) {
            $file = $request->file('file');
            $file->storeAs(UploadController::getUploadDirectory($request->id), $file->getClientOriginalName(), 'public');
        }

        return response('OK', 200);
    }

    /**
     * Complete an upload by creating a collection and adding all
     * files to it.
     */
    public function complete(Request $request)
    {
        if ($request->has('id')) {
            $collection = Collection::create([
                'name' => $request->collection ?? 'Upload ' . Carbon::now()->toDateTimeString(),
            ]);
            $collection->users()->attach(Auth::user());

            collect(Storage::disk('public')
                ->files(UploadController::getUploadDirectory($request->id)))
                ->each(function ($file) use ($collection) {
                    ProcessNewAsset::dispatch($collection, $file);
                });
        }

        return redirect()->route('home.index');
    }

    /**
     * Cancel an upload and destroy the temporary directory.
     */
    public function destroy(Request $request)
    {
        if ($request->has('id')) {
            Storage::disk('public')->deleteDirectory(UploadController::getUploadDirectory($request->id));
        }

        return redirect()->route('home.index');
    }

    private static function getUploadDirectory(string $uploadId)
    {
        return UploadController::UPLOADS_DIRECTORY . '/' . $uploadId;
    }
}
