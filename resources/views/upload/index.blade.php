@extends('layouts.app')

@section('title', 'Upload')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="bi bi-house"></i> <a href="{{ route('collections.index') }}">{{ __('nav.home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('nav.upload') }}</li>
        </ol>
    </nav>

    <h1>{{ __('upload.title') }}</h1>

    <form action="{{ route('upload.store') }}" class="dropzone" id="u">
        @csrf
        <input type="hidden" name="id" value="{{ $uploadId }}">
    </form>

    <hr>

    <form action="{{ route('upload.complete') }}" method="post" class="row mb-3">
        @csrf
        <input type="hidden" name="id" value="{{ $uploadId }}">
        <div class="col-auto">
            <label for="collection" class="col-form-label">{{ __('upload.collection_name') }}</label>
        </div>
        <div class="col-auto">
            <div class="input-group">
                <input type="text" class="form-control" id="collection" name="collection" required value="Upload {{ Carbon\Carbon::now()->toDatetimeString() }}">
                <button type="submit" class="btn btn-success">{{ __('upload.save') }}</button>
            </div>
        </div>

    </form>

    <form action="{{ route('upload.destroy') }}" method="post">
        @csrf
        @method('DELETE')
        <input type="hidden" name="id" value="{{ $uploadId }}">
        <button class="btn btn-danger">{{ __('upload.cancel') }}</button>
    </form>

    <script>
        document.addEventListener('DOMContentLoaded', () => {
            Dropzone.options.u = {
                'acceptedFiles': 'image/*',
                'dictDefaultMessage': '{{ __('upload.drop_files') }}',
                'maxFilesize': {{ config('upload.max_file_size') }},
                'init': function() {
                    // Attempt to prevent directory upload
                    this.hiddenFileInput.setAttribute("webkitdirectory", false);
                }
            };
        });
    </script>
@endsection
