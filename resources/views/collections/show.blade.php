@extends('layouts.app')

@section('title', "Collection {$collection->name}")

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="bi bi-house"></i> <a href="{{ route('collections.index') }}">{{ __('nav.home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $collection->name }}</li>
        </ol>
    </nav>

    <form action="{{ route('collections.destroy', $collection) }}" method="post"
        onsubmit="javascript:return confirm('This collection and all assets will be permanently deleted');">
        @method('DELETE')
        @csrf
        <h1>
            {{ $collection->name }}

            @can('delete', $collection)
                <button type="submit" class="btn btn-link text-danger">
                    <i class="bi bi-trash"></i>
                </button>
            @endcan
        </h1>
    </form>

    {{ $assets->links() }}
    <hr>

    <div class="mt-1 row row-cols-1 row-cols-sm-2 row-cols-xl-4 g-3">
        @foreach ($assets as $asset)
            @can('view', $asset)
                <div class="col d-flex">
                    @include('assets.partial-list', ['asset' => $asset])
                </div>
            @endcan
        @endforeach
    </div>

    <hr>
    {{ $assets->links() }}

@endsection
