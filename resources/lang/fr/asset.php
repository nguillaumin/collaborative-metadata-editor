<?php

return [
    'rotate_left' => 'Rotation gauche',
    'rotate_right' => 'Rotation droite',

    'name' => 'Nom',
    'type' => 'Type',
    'datetime' => 'Date',
    'date' => 'Datation',
    'date_placeholder' => 'ex: "1985", ou "1985-86"',
    'location' => 'Lieu',
    'description' => 'Description',
    'tags' => 'Tags',
    'tag_enter' => 'Saisissez un tag…',

    'save' => 'Enregistrer',

    'no_date' => 'Pas de métadonnée date dans le ficher',

    'processing_failed' => 'Le traitement de cette photo a échoué',
    'unknown_reason' => 'Raison inconnue',
    'retry' => 'Réessayer',
];
