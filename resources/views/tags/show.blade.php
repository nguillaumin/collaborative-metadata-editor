@extends('layouts.app')

@section('title', "Tag {$tag->name}")

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="bi bi-house"></i> <a href="{{ route('collections.index') }}">{{ __('nav.home') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('tags.index') }}">{{ __('nav.tags') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $tag->name }}</li>
        </ol>
    </nav>

    <h1>{!! __('tags.title', ['tag' => $tag->name ]) !!}</h1>

    <p>
        {{ __('tags.intro', ['count' => $tag->assets->count(), 'access' => $assets->count() ]) }}
    </p>

    <div class="mt-1 row row-cols-1 row-cols-sm-2 row-cols-xl-4 g-3">
        @foreach ($assets as $asset)
            @can('view', $asset)
                <div class="col d-flex">
                    @include('assets.partial-list', ['asset' => $asset])
                </div>
            @endcan
        @endforeach
    </div>

@endsection
