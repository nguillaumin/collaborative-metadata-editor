@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col">

            <div class="row">
                <h1 class="offset-md-4">{{ __('auth.register') }}</h1>
            </div>

            <form action="{{ $user ? route('register_invite') : route('register') }}" method="post" class="mt-3">
                @csrf
                <input type="hidden" name="invitation_token" value="{{ $user?->invitation_token }}">

                @error('invitation_token')
                    <div class="row mb-3">
                        <div class="col-md-5 offset-md-4">
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        </div>
                    </div>
                @enderror


                @if ($firstUser)
                    <div class="alert alert-warning">
                        {!! __('auth.register_first') !!}
                    </div>
                @endif

                <div class="row mb-3">
                    <label for="name" class="form-label col-md-4 text-md-end">{{ __('auth.name') }}</label>
                    <div class="col-md-5">
                        <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" name="name"
                            value="{{ old('name') }}" autofocus required>

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="email" class="form-label col-md-4 text-md-end">{{ __('auth.email') }}</label>
                    <div class="col-md-5">
                        @if ($user)
                            <input type="email" readonly class="form-control" id="email" value="{{ $user->email }}">
                        @else
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                                name="email" value="{{ old('email') }}" autofocus required>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="password" class="form-label col-md-4 text-md-end">{{ __('auth.password') }}</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password"
                            name="password" autocomplete="current-password" required>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="password_confirmation" class="form-label col-md-4 text-md-end">{{ __('auth.password_confirmation') }}</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                            id="password_confirmation" name="password_confirmation" autocomplete="new-password" required>

                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">{{ __('auth.register_submit') }}</button>

                        <a class="btn btn-link" href="{{ route('login') }}">
                            {{ __('auth.already_registered') }}
                        </a>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
