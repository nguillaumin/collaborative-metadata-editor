<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessNewAsset;
use App\Models\Asset;
use App\Models\ProcessLog;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Asset::class, 'asset');
    }

    public function show(Asset $asset)
    {
        $collectionImages = DB::table('assets')
            ->select('id')
            ->where('collection_id', '=', $asset->collection->id)
            ->orderBy('name')
            ->pluck('id');

        $assetIndex = $collectionImages->search($asset->id);
        $previous = null;
        $next = null;
        if ($assetIndex > 0) {
            $previous = $collectionImages[$assetIndex - 1];
        }
        if ($assetIndex < count($collectionImages) - 1) {
            $next = $collectionImages[$assetIndex + 1];
        }

        $tags = Tag::orderBy('name')->get();

        return view('assets.show', [
            'asset' => $asset,
            'previous' => $previous,
            'next' => $next,
            'tags' => $tags,
        ]);
    }

    public function update(Request $request, Asset $asset)
    {
        $asset->update([
            'name' => $request->name,
            'location' => $request->location,
            'description' => $request->description,
            'date' => $request->date,
        ]);

        $asset->tags()->detach();

        if ($request->has('tags')) {
            foreach ($request->tags as $tag) {
                $existing = Tag::where('name', '=', $tag)->get();
                if ($existing->isEmpty()) {
                    $existing = Tag::create([
                        'name' => $tag,
                    ]);
                } else {
                    $existing = $existing->first();
                }
                $asset->tags()->save($existing);
            }
        }

        return redirect()->route('assets.show', $asset);
    }

    public function process(Asset $asset)
    {
        if (! $asset->processed) {
            try {
                $asset->processLog?->delete();
                ProcessNewAsset::processAsset($asset);
            } catch (Exception $e) {
                $log = new ProcessLog([
                    'log' => $e->getMessage(),
                ]);
                $asset->processLog()->save($log);
            }
        }

        return redirect()->route('assets.show', $asset);
    }

    public function rotate(Request $request, Asset $asset)
    {
        $path = Storage::disk('public')->path($asset->original_path);

        switch ($request->direction) {
            case 'left':
                $img = ImageManagerStatic::make($path);
                $img->rotate(90);
                $img->save();
                ProcessNewAsset::createThumbnail($asset, $img);
                break;
            case 'right':
                $img = ImageManagerStatic::make($path);
                $img->rotate(-90);
                $img->save();
                ProcessNewAsset::createThumbnail($asset, $img);
                break;
        }

        return redirect()->route('assets.show', $asset);
    }

    public function destroy(Asset $asset)
    {
        Storage::disk('public')->delete($asset->original_path);
        Storage::disk('public')->delete($asset->thumbnail_path);

        $asset->delete();

        return redirect()->route('collections.show', $asset->collection);
    }
}
