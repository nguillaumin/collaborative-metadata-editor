<?php

return [

    'failed' => 'Identifiant ou mot de passe incorrect.',
    'password' => 'Mot de passe incorrect.',
    'throttle' => 'Trop de tentatives de connexion. Veuillez r\'éessayer dans :seconds secondes.',

    'login' => 'Connexion',
    'logout' => 'Déconnexion',
    'email' => 'Email',
    'password' => 'Mot de passe',
    'password_confirmation' => 'Confirmation du mot de passe',
    'name' => 'Nom',

    'remember_me' => 'Rester connecté',
    'forgot_password' => 'Mot de passe oublié',
    'register' => 'Inscription',
    'register_submit' => 'S\'inscrire',
    'already_registered' => 'Déjà inscrit?',

    'register_first' => 'Inscription du tout premier utilisateur. Ce nouvel utilisateur obtiendra les <strong>permissions d\'administration</strong> de l\'application',

    'forgot_password_intro' => 'Mot de passe oublié? Pas de problème. Indiquez votre email ci-dessous et nous vous enverrons un lien pour réinitialiser votre mot de passe.',
    'forgot_password_submit' => 'Envoyer un lien de réinitialisation',
    'reset_password' => 'Réinitialiser le mot de passe',

    'verify_email_intro' => 'Merci pour votre inscription! Avant de commencer, veuillez vérifier votre addresse email en cliquant sur le lien qui vous a été envoyé.',
    'verify_email_resend' => 'Renvoyer l\'email de vérification',
    'verify_email_resent' => 'Un nouveau email de vérification à été envoyé.',

];
