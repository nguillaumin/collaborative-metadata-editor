# Collaborative Metadata Editor

A simple gallery application to allow users to edit image metadata.

Once the edition is complete, images are intended to be exported with their metadata embedded in EXIF tags or in companion XMP files.

## Development

This is a Laravel application. Refer to the Laravel documentation for more details.

The instructions below use Laravel Sail to run the application with Docker.

- Copy `.env.example` file to `.env`
- Install the PHP dependencies: `docker run --rm -v $(pwd):/opt -w /opt laravelsail/php80-composer:latest composer install`
- Start Sail: `./vendor/bin/sail up -d` (This will take some time the first time)

Once started:
- Run the migrations: `./vendor/bin/sail artisan migrate`
- Generate app key: `./vendor/bin/sail artisan key:generate`
- Install frontend dependencies: `./vendor/bin/sail npm install`
- Run Laravel Mix to generate frontend assets: `./vendor/bin/sail npm run dev`
- Generate the public storage link: `./vendor/bin/sail artisan storage:link`
