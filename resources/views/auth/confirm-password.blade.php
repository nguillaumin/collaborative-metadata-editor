@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col">

            <p>This is a secure area of the application. Please confirm your password before continuing.</p>

            <form action="{{ route('password.confirm') }}" method="post" class="mt-3">
                @csrf

                <div class="row mb-3">
                    <label for="password" class="form-label col-md-4 text-md-end">Password</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password"
                            autocomplete="current-password" required>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
