@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col">
            <p class="mt-3">{{ __('auth.forgot_password_intro') }}</p>

            <form action="{{ route('password.email') }}" method="post" class="mt-3">
                @csrf

                <div class="row mb-3">
                    <label for="email" class="form-label col-md-4 text-md-end">{{ __('auth.email') }}</label>
                    <div class="col-md-5">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}"
                            autofocus required>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">{{ __('auth.forgot_password_submit') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
