<?php

return [
    'rotate_left' => 'Rotate 90° counter-clockwise',
    'rotate_right' => 'Rotate 90° clockwise',

    'name' => 'Name',
    'type' => 'Type',
    'datetime' => 'Date',
    'date' => 'Date (approx.)',
    'date_placeholder' => 'e.g.: "1985", or "1985-86"',
    'location' => 'Location',
    'description' => 'Description',
    'tags' => 'Tags',
    'tag_enter' => 'Enter a tag…',

    'save' => 'Save',

    'no_date' => 'File does not have date metadata',

    'processing_failed' => 'This asset failed to process',
    'unknown_reason' => 'Unknown reason',
    'retry' => 'Try again',
];
