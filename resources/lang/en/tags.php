<?php

return [
    'tag_count' => '{0}No tags|[1]:count tag|[2,*]:count tags',
    'title' => 'Tag: <code>:tag</code>',
    'intro' => ':count assets tagged, :access of which you have access to.',
];
