@extends('layouts.app')

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="bi bi-house"></i> <a href="{{ route('collections.index') }}">{{ __('nav.home') }}</a></li>
            <li class="breadcrumb-item"><a href="{{ route('collections.show', $asset->collection) }}">{{ $asset->collection->name }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $asset->name }}</li>
        </ol>
    </nav>

    @include('assets.partial-nav')

    <hr>


    <form action="{{ route('assets.destroy', $asset) }}" method="post"
        onsubmit="javascript:return confirm('This asset will be permanently deleted');">
        @method('DELETE')
        @csrf
        <h1>
            {{ $asset->name }}

            @can('delete', $asset)
                <button type="submit" class="btn btn-link text-danger">
                    <i class="bi bi-trash"></i>
                </button>
            @endcan
        </h1>
    </form>


    <form action="{{ route('assets.rotate', $asset) }}" class="my-2" method="post">
        @csrf
        @method('PUT')

        <button type="submit" name="direction" value="left" class="btn btn-light" title="{{ __('asset.rotate_left') }}">
            <i class="bi bi-arrow-counterclockwise"></i>
        </button>
        <button type="submit" name="direction" value="right" class="btn btn-light" title="{{ __('asset.rotate_right') }}">
            <i class="bi bi-arrow-clockwise"></i>
        </button>
    </form>


    <div class="row row-cols-1 row-cols-md-2">
        <div class="col text-center">
            <a data-fslightbox href="{{ asset("storage/{$asset->original_path}") }}?ts={{ microtime(true) }}">
                <img src="{{ asset("storage/{$asset->original_path}") }}?ts={{ microtime(true) }}" class="img-fluid border">
            </a>
        </div>
        <div class="col">
            @if ($asset->processed)
                <form action="{{ route('assets.update', $asset) }}" method="post">
                    @csrf
                    @method('PUT')

                    <div class="mb-3">
                        <label for="name" class="form-label text-muted">{{ __('asset.name') }}</label>
                        <input type="text" id="name" name="name" class="form-control" value="{{ $asset->name ?? '' }}">
                    </div>
                    <div class="mb-3">
                        <label for="extension" class="form-label text-muted">{{ __('asset.type') }}</label>
                        <input type="text" readonly class="form-control-plaintext"
                            value="{{ strtoupper($asset->extension) }}">
                    </div>
                    <div class="mb-3">
                        <label for="datetime" class="form-label text-muted">{{ __('asset.datetime') }}</label>
                        <input type="text" readonly class="form-control-plaintext" value="{{ $asset->datetime ?? __('asset.no_date') }}">
                    </div>
                    <div class="mb-3">
                        <label for="date" class="form-label text-muted">{{ __('asset.date') }}</label>
                        <input type="text" id="date" name="date" class="form-control" placeholder="{{ __('asset.date_placeholder') }}" value="{{ $asset->date ?? '' }}">
                    </div>
                    <div class="mb-3">
                        <label for="location" class="form-label text-muted">{{ __('asset.location') }}</label>
                        <input type="text" id="name" name="location" class="form-control"
                            value="{{ $asset->location ?? '' }}">
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label text-muted">{{ __('asset.description') }}</label>
                        <textarea class="form-control" id="description" name="description"
                            rows="3">{{ $asset->description ?? '' }}</textarea>
                    </div>
                    <div class="mb-3">
                        <label for="tags" class="form-label text-muted">{{ __('asset.tags') }}</label>
                        <select class="form-select" id="tags" name="tags[]" multiple data-allow-new="true"
                            data-show-all-suggestions="true" data-allow-clear="true">
                            <option selected disabled hidden value="">{{ __('asset.tag_enter') }}</option>
                            @foreach ($tags as $tag)
                                <option value="{{ $tag->name }}" @if ($asset->tags->contains($tag)) selected @endif>{{ $tag->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success">{{ __('asset.save') }}</button>
                </form>
            @else
                <div class="alert alert-warning" role="alert">
                    <h4>{{ __('asset.processing_failed') }} <i class="bi bi-emoji-frown"></i></h4>
                    @if ($asset->processLog)
                        <p>{{ $asset->processLog->updated_at }}</p>
                        <p class="font-monospace mt-3">{{ $asset->processLog->log }}</p>
                    @else
                        <p>{{ __('asset.unknown_reason') }}</p>
                    @endif

                    <form action="{{ route('assets.process', $asset) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary">{{ __('asset.retry') }}</button>
                    </form>
                </div>

            @endif
        </div>
    </div>

    <hr>

    @include('assets.partial-nav')

@endsection
