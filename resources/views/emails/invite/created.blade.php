<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Invitation</title>
</head>

<body>
    <p>
        Hi,
    </p>

    <p>
        You have been invited to collaborate on metadata for a gallery of assets.
        Please follow
        <a href="{{ route('register', ['invitation_token' => $user->invitation_token]) }}">this link</a>
        to register your account.
    </p>

    <p>
        If you do not wish to participate, no further action is required.
    </p>
</body>

</html>
