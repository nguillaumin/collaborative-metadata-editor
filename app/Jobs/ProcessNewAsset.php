<?php

namespace App\Jobs;

use App\Models\Asset;
use App\Models\Collection;
use App\Models\ProcessLog;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use Intervention\Image\ImageManagerStatic;

class ProcessNewAsset implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Collection $collection;
    private string $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $collection, string $file)
    {
        $this->collection = $collection->withoutRelations();
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asset = new Asset([
            'name' => File::name($this->file),
            'extension' => strtolower(File::extension($this->file)),
        ]);
        $this->collection->assets()->save($asset);

        try {
            Storage::disk('public')
                ->move($this->file, $asset->original_path);

            ProcessNewAsset::processAsset($asset);
        } catch (Exception $e) {
            $log = new ProcessLog([
                'log' => $e->getMessage(),
            ]);
            $asset->processLog()->save($log);
        }
    }

    public static function processAsset(Asset $asset)
    {
        $image = ImageManagerStatic::make(
            Storage::disk('public')->path($asset->original_path)
        );

        ProcessNewAsset::createThumbnail($asset, $image);

        $asset->description = $image->exif('ImageDescription');
        if ($image->exif('DateTime')) {
            $asset->datetime = Carbon::parse($image->exif('DateTime'));
        }
        $asset->processed = true;
        $asset->save();
    }

    public static function createThumbnail(Asset $asset, Image $image)
    {
        Storage::disk('public')
            ->put(
                $asset->thumbnail_path,
                $image
                    ->widen(320)
                    ->orientate()
                    ->stream()
            );
    }
}
