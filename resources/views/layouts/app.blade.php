<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', 'Metadata editor')</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>


    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand">Metadata Editor</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link {{ Request::routeIs('collections.*') ? 'active' : ''}}" href="{{ route('collections.index') }}"><i class="bi bi-collection"></i> {{ __('nav.collections') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::routeIs('upload.*') ? 'active' : ''}}" href="{{ route('upload.index') }}"><i class="bi bi-upload"></i> {{ __('nav.upload') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ Request::routeIs('tags.*') ? 'active' : ''}}" href="{{ route('tags.index') }}"><i class="bi bi-tags"></i> {{ __('nav.tags') }}</a>
                        </li>
                        @can('viewAny', App\Models\User::class)
                            <li class="nav-item">
                                <a class="nav-link {{ Request::routeIs('users.*') ? 'active' : ''}}" href="{{ route('users.index') }}"><i class="bi bi-people"></i> {{ __('nav.users') }}</a>
                            </li>
                        @endcan
                    </ul>
                    <ul class="navbar-nav ms-auto">
                        @auth
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="profile" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="profile">
                                <li>
                                    <form action="{{ route('logout') }}" method="post">
                                        @csrf
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault(); this.closest('form').submit();">{{ __('auth.logout') }}</a>
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endauth
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('auth.register') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('auth.login') }}</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
