<?php

namespace App\Http\Controllers;

use App\Mail\InviteCreated;
use App\Models\Collection;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    public function index()
    {
        $users = User::orderBy('name')->get();

        return view('users.index', [
            'users' => $users,
        ]);
    }

    public function edit(User $user)
    {
        $collections = Collection::all()
            ->sortBy('name');

        return view('users.edit', [
            'user' => $user,
            'collections' => $collections,
        ]);
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'role' => [Rule::in('', User::ROLE_ADMIN)],
        ]);

        if ($request->filled('name')) {
            $user->name = $request->name;
        }
        if ($request->filled('email')) {
            $user->email = $request->email;
        }
        if ($request->filled('role')) {
            $user->role = $request->role;
        } else {
            $user->role = null;
        }

        $user->collections()->detach();
        if ($request->filled('collections')) {
            foreach ($request->collections as $collectionId) {
                $collection = Collection::find($collectionId);
                if ($collection) {
                    $user->collections()->attach($collection);
                }
            }
        }

        $user->save();

        return redirect()->route('users.edit', $user);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }

    public function invite()
    {
        $collections = Collection::all()
            ->sortBy('name');

        return view('users.invite', [
            'collections' => $collections,
        ]);
    }

    public function sendInvite(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'collections' => 'required',
        ]);

        $user = User::create([
            'name' => $request->email,
            'email' => $request->email,
            'invitation_token' => Str::random(32),
            'invited_at' => Carbon::now(),
            'password' => Str::random(255),
        ]);

        if ($request->filled('collections')) {
            foreach ($request->collections as $collectionId) {
                $collection = Collection::find($collectionId);
                if ($collection) {
                    $user->collections()->attach($collection);
                }
            }
        }
        $user->save();

        Mail::to($user->email)->send(new InviteCreated($user));

        return redirect()->route('users.index');
    }
}
