<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;

    const PATH_ORIGINALS = 'originals';
    const PATH_THUMBNAILS = 'thumbnails';

    protected $fillable = [
        'name', 'extension', 'description',
        'location', 'datetime', 'date',
    ];

    protected $casts = [
        'datetime' => 'datetime',
        'processed' => 'boolean',
    ];

    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function processLog()
    {
        return $this->hasOne(ProcessLog::class);
    }

    public function getBasenameAttribute()
    {
        return str_pad($this->id, 12, '0', STR_PAD_LEFT);
    }

    public function getFilenameAttribute()
    {
        return $this->basename . '.' . strtolower($this->extension);
    }

    public function getStoragePath()
    {
        $id = str_pad($this->id, 12, '0', STR_PAD_LEFT);
        $chunks = str_split($id, 3);

        return join('/', [
            $chunks[0],
            $chunks[1],
            $chunks[2],
            $this->filename,
        ]);
    }

    public function getOriginalPathAttribute()
    {
        return Asset::PATH_ORIGINALS . '/' . $this->getStoragePath();
    }

    public function getThumbnailPathAttribute()
    {
        return Asset::PATH_THUMBNAILS . '/' . $this->getStoragePath();
    }

    public function getThumbnailUrlAttribute()
    {
        if ($this->processed) {
            return asset('storage/' . $this->thumbnail_path);
        } else {
            return '/img/placeholder.png';
        }
    }
}
