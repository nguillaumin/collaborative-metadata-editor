<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' => 'Log in',
    'logout' => 'Log out',
    'email' => 'Email address',
    'password' => 'Password',
    'password_confirmation' => 'Confirm Password',
    'name' => 'Name',

    'remember_me' => 'Remember me',
    'forgot_password' => 'Forgot your password?',
    'register' => 'Create an account',
    'register_submit' => 'Register',
    'already_registered' => 'Already registered?',

    'register_first' => 'You are registering the very first user of the application. This new user will <strong>get administrative permissions</strong>',

    'forgot_password_intro' => 'Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.',
    'forgot_password_submit' => 'Email Password Reset Link',
    'reset_password' => 'Reset Password',

    'verify_email_intro' => 'Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.',
    'verify_email_resend' => 'Resend Verification Email',
    'verify_email_resent' => 'A new verification link has been sent to the email address you provided during registration.',

];
