@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col">

            <div class="row">
                <h1 class="offset-md-4">{{ __('auth.login') }}</h1>
            </div>

            <form action="{{ route('login') }}" method="post" class="mt-3">
                @csrf

                <div class="row mb-3">
                    <label for="email" class="form-label col-md-4 text-md-end">{{ __('auth.email') }}</label>
                    <div class="col-md-5">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}"
                            autofocus required>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="password" class="form-label col-md-4 text-md-end">{{ __('auth.password') }}</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password"
                            autocomplete="current-password" required>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-5 offset-md-4">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="remember" name="remember">
                            <label class="form-check-label" for="remember">{{ __('auth.remember_me') }}</label>
                        </div>
                    </div>
                </div>

                <div class="row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">{{ __('auth.login') }}</button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('auth.forgot_password') }}
                            </a>
                        @endif

                        @if (Route::has('register'))
                            <a class="btn btn-link ps-0" href="{{ route('register') }}">
                                {{ __('auth.register') }}
                            </a>
                        @endif
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
