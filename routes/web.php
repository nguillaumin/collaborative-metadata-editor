<?php

use App\Http\Controllers\AssetController;
use App\Http\Controllers\CollectionController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('collections.index');
})->name('home.index');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/collections', [CollectionController::class, 'index'])->name('collections.index');
    Route::get('/collections/{collection}', [CollectionController::class, 'show'])->name('collections.show');
    Route::delete('/collections/{collection}', [CollectionController::class, 'destroy'])->name('collections.destroy');

    Route::get('/upload', [UploadController::class, 'index'])->name('upload.index');
    Route::post('/upload', [UploadController::class, 'store'])->name('upload.store');
    Route::post('/upload/complete', [UploadController::class, 'complete'])->name('upload.complete');
    Route::delete('/upload', [UploadController::class, 'destroy'])->name('upload.destroy');

    Route::get('/assets/{asset}', [AssetController::class, 'show'])->name('assets.show');
    Route::put('/assets/{asset}', [AssetController::class, 'update'])->name('assets.update');
    Route::delete('/asset/{asset}', [AssetController::class, 'destroy'])->name('assets.destroy');
    Route::put('/assets/{asset}/rotate', [AssetController::class, 'rotate'])->name('assets.rotate');
    Route::post('/assets/process/{asset}', [AssetController::class, 'process'])->name('assets.process');

    Route::get('/tags', [TagController::class, 'index'])->name('tags.index');
    Route::get('/tags/{tag}', [TagController::class, 'show'])->name('tags.show');

    Route::get('invite', [UserController::class, 'invite'])->name('users.invite');
    Route::post('invite', [UserController::class, 'sendInvite'])->name('users.send_invite');

    Route::resource('users', UserController::class);
});

require __DIR__ . '/auth.php';
