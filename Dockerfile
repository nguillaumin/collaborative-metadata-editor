FROM php:8-apache

RUN apt-get update && apt-get install -y \
  build-essential \
  libpng-dev \
  libzip-dev \
  libjpeg62-turbo-dev \
  libpq-dev \
  libsqlite3-dev

RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install pdo pdo_mysql pdo_pgsql pdo_sqlite gd exif zip

RUN a2enmod rewrite

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN echo "memory_limit = 512M" > $PHP_INI_DIR/conf.d/memory.ini
RUN echo "upload_max_filesize = 32M" >> $PHP_INI_DIR/conf.d/upload.ini
RUN echo "post_max_size = 32M" >> $PHP_INI_DIR/conf.d/upload.ini

WORKDIR /var/www/html
COPY --chown=www-data:www-data . /var/www/html
