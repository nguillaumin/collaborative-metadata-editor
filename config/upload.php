<?php

return [
    'max_file_size' => env('MAX_UPLOAD_FILE_SIZE_MB', 32),
];
