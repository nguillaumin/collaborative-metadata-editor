<div class="card w-100">
    <a href="{{ route('assets.show', $asset) }}">
        <img src="{{ $asset->thumbnail_url }}?ts={{ microtime(true) }}" class="card-img-top"
            alt="{{ $asset->name }}">
    </a>
    <div class="card-body">
        <div class="card-title">
            {{ $asset->name }}.{{ $asset->extension }}
            @if (!$asset->processed)
                <i class="bi bi-exclamation-triangle-fill text-warning"></i>
            @endif
        </div>
        <div class="card-text">
            @if ($asset->tags->isNotEmpty())
                <ul class="list-inline">
                    @foreach ($asset->tags as $tag)
                        <span class="badge bg-primary">{{ $tag->name }}</span>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
