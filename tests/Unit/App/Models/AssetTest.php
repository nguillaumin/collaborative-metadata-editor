<?php

namespace Tests\Unit\App\Model;

use App\Models\Asset;
use PHPUnit\Framework\TestCase;

class AssetTest extends TestCase
{
    public function test_filename_attribute()
    {
        $asset = new Asset([
            'name' => 'filename',
            'extension' => 'ext',
        ]);
        $asset->id = 1;

        $this->assertEquals('000000000001.ext', $asset->filename);
    }

    public function test_storage_path()
    {
        $asset = new Asset([
            'name' => 'filename',
            'extension' => 'ext',
        ]);
        $asset->id = 1;

        $this->assertEquals('000/000/000/000000000001.ext', $asset->getStoragePath());

        $asset->id = 123;
        $this->assertEquals('000/000/000/000000000123.ext', $asset->getStoragePath());

        $asset->id = 123456;
        $this->assertEquals('000/000/123/000000123456.ext', $asset->getStoragePath());

        $asset->id = 123456789;
        $this->assertEquals('000/123/456/000123456789.ext', $asset->getStoragePath());

        $asset->id = 12345678901234;
        $this->assertEquals('123/456/789/12345678901234.ext', $asset->getStoragePath());
    }
}
