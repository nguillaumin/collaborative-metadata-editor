@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col">

            <p class="mt-3">{{ __('auth.verify_email_intro') }}</p>

            @if (session('status') == 'verification-link-sent')
                <div class="alert alert-success" role="alert">
                    {{ __('auth.verify_email_resent') }}
                </div>
            @endif

            <form action="{{ route('verification.send') }}" method="post" class="mt-3">
                @csrf

                <div class="row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">{{ __('auth.verify_email_resend') }}</button>
                        <a class="btn btn-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('auth.logout') }}
                        </a>
                    </div>
                </div>

            </form>

            <form method="post" action="{{ route('logout') }}" id="logout-form">
                @csrf
            </form>
        </div>
    </div>
@endsection
