@extends('layouts.app')

@section('title', "Tags")

@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><i class="bi bi-house"></i> <a href="{{ route('collections.index') }}">{{ __('nav.home') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ __('nav.tags') }}</li>
        </ol>
    </nav>

    <h1>{{ trans_choice('tags.tag_count', $tags->count()) }}</h1>

    @if ($tags->isNotEmpty())
        <ul class="list-inline">
            @foreach ($tags as $tag)
                <span class="badge bg-primary">
                    <a class="text-white" href="{{ route('tags.show', $tag) }}">{{ $tag->name }}</a>
                    <span class="text-warning ms-2">{{ $tag->assets->count() }}</span>
                </span>
            @endforeach
        </ul>
    @endif

@endsection
