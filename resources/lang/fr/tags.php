<?php

return [
    'tag_count' => '{0}Pas de tags|[1]:count tag|[2,*]:count tags',
    'title' => 'Tag: <code>:tag</code>',
    'intro' => ':count photos taguées, :access vous sont acessibles.',
];
