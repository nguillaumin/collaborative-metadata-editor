@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col">
            <form action="{{ route('password.update') }}" method="post" class="mt-3">
                @csrf
                <input type="hidden" name="token" value="{{ $request->route('token') }}">

                <div class="row mb-3">
                    <label for="email" class="form-label col-md-4 text-md-end">{{ __('auth.email') }}</label>
                    <div class="col-md-5">
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email', $request->email) }}"
                            autofocus required>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="password" class="form-label col-md-4 text-md-end">{{ __('auth.password') }}</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password"
                            autocomplete="current-password" required>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="password_confirmation" class="form-label col-md-4 text-md-end">{{ __('auth.password_confirmation') }}</label>
                    <div class="col-md-5">
                        <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation"
                            autocomplete="new-password" required>

                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="submit" class="btn btn-primary">{{ __('auth.reset_password') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
