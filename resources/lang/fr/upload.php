<?php

return [
    'title' => 'Envoyer des fichiers',
    'drop_files' => 'Déposer ici pour envoyer',
    'collection_name' => 'Nom de la collection',
    'save' => 'Enregistrer',
    'cancel' => 'Annuler',
    'file_too_large' => 'Fichier trop gros, doit être < :limit MB',
];
